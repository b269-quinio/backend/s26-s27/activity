

// USERS

{
	"FirstName": "Joseph Nathaniel",
	"Last Name": "Quinio",
	"Email": "natquinio@gmail.com",
	"Password": "password123",
	"Is Admin?": "Yes",
	"Mobile Number": "09123456789"
},

// ORDERS

{
	"User Id": "NQ001",
	"Transaction Date": "03272023",
	"Status": "Awaiting approval of Quotation",
	"Total": "PHP 17,280.00"
},

//PRODUCTS

{
	{
		"Name": "Boysen Flat Latex",
		"Description": "Architectural - White Paint",
		"Price": "PHP 560.00/gal",
		"Stocks": "200 gallons",
		"Is Active?": "Yes",
		"SKU": "ZG011AQA"
	},

	{
		"Name": "Boysen Semi-Gloss",
		"Description": "Architectural - Black Paint",
		"Price": "PHP 740.00/gal",
		"Stocks": "120 gallons",
		"Is Active?": "Yes",
		"SKU": "TG103AZA"
	}
},

// ORDER PRODUCTS

{
	{
		"Order ID": "P0 0001",
		"Product ID": "FLATAQA011",
		"Quantity": "15 gallons",
		"Price": "PHP 560.00/gal",
		"Sub Total": "PHP 8,400.00"
	},

	{
		"Order ID": "P0 0002",
		"Product ID": "SEMIAZA103",
		"Quantity": "12 gallons",
		"Price": "PHP 740.00/gal",
		"Sub Total": "PHP 8,880.00"
	}
}